This is the start of a test suite for Drupal.org. It is very basic and is based on Rspec, an awesome BDD system for Ruby.

Here are some goals of the system:
  1) Ensure front-end functionality triggers backend events (i.e. sandbox creation leads to a Git repo).
  2) Allow for users to begin to test their development code to Drupal.org itself and related properties.
  3) Ensure transactional functionality of Twisted, Supervisord, and Beanstalkd.
      -Be sure the SSH system for Git is accepting connections and routing them properly.
      -Basic message bus tests; create a message at one point and retrieve it later.
